

\documentclass[12pt]{article}



% *** PDF, URL AND HYPERLINK PACKAGES ***
%
\usepackage{amssymb}
\usepackage{amsmath}            % For \numberwithin
\usepackage{url}
\usepackage{bm}
\usepackage{color}
\usepackage{verbatim}           % For comments
\usepackage{fancyvrb}
\usepackage{graphicx}           % Standard graphics package
\usepackage{algorithm}
\usepackage{algorithmicx}
\usepackage{algpseudocode}
\usepackage{mathabx}
\usepackage{hyperref}
\input{latex-commands}

\textwidth 6.5in
\oddsidemargin 0in
\evensidemargin 0in


% *** Do not adjust lengths that control margins, column widths, etc. ***
% *** Do not use packages that alter fonts (such as pslatex).         ***
% There should be no need to do such things with IEEEtran.cls V1.6 and later.
% (Unless specifically asked to do so by the journal or conference you plan
% to submit to, of course. )


% correct bad hyphenation here
\hyphenation{op-tical net-works semi-conduc-tor}

% IEEEtran has huge spaces between bibliography items. This trick lets
% us reduce them to save space.
\let\oldthebibliography=\thebibliography
\let\endoldthebibliography=\endthebibliography
\renewenvironment{thebibliography}[1]{%
  \begin{oldthebibliography}{#1}%
    %\setlength{\itemsep}{10pt plus 2.5pt minus 2.5pt} % old value
    \setlength{\itemsep}{0pt} % adjust as necessary
}{%
  %\bigskip {\bf itemsep \the\itemsep} % activate this line to find out what was set
  \end{oldthebibliography}%
}


\begin{document}
%
% paper title
% can use linebreaks \\ within to get better formatting as desired
\title{Accurately Citing Software and Algorithms\\ Used in Publications}
% author names and affiliations
% use a multiple column layout for up to two different
% affiliations

\author{
  Matthew G. Knepley \\
  {\small Computation Institute}\\
  {\small University of Chicago}\\
  {\small Chicago, IL, USA}\\
  {\small \tt knepley@ci.uchicago.edu}
\and
  Jed Brown, Lois Curfman McInnes, and Barry Smith \\
  {\small Mathematics and Computer Science Division} \\
  {\small Argonne National Laboratory}\\
  {\small Argonne, IL, USA}\\
  {\small \tt [jedbrown,mcinnes,bsmith]@mcs.anl.gov}
}

% conference papers do not typically use \thanks and this command
% is locked out in conference mode. If really needed, such as for
% the acknowledgment of grants, issue a \IEEEoverridecommandlockouts
% after \documentclass

% for over three affiliations, or if they all won't fit within the width
% of the page, use this alternative format:
% 
%\author{\IEEEauthorblockN{Michael Shell\IEEEauthorrefmark{1},
%Homer Simpson\IEEEauthorrefmark{2},
%James Kirk\IEEEauthorrefmark{3}, 
%Montgomery Scott\IEEEauthorrefmark{3} and
%Eldon Tyrell\IEEEauthorrefmark{4}}
%\IEEEauthorblockA{\IEEEauthorrefmark{1}School of Electrical and Computer Engineering\\
%Georgia Institute of Technology,
%Atlanta, Georgia 30332--0250\\ Email: see http://www.michaelshell.org/contact.html}
%\IEEEauthorblockA{\IEEEauthorrefmark{2}Twentieth Century Fox, Springfield, USA\\
%Email: homer@thesimpsons.com}
%\IEEEauthorblockA{\IEEEauthorrefmark{3}Starfleet Academy, San Francisco, California 96678-2391\\
%Telephone: (800) 555--1212, Fax: (888) 555--1212}
%\IEEEauthorblockA{\IEEEauthorrefmark{4}Tyrell Inc., 123 Replicant Street, Los Angeles, California 90210--4321}}




% use for special paper notices
%\IEEEspecialpapernotice{(Invited Paper)}




% make the title area
\maketitle


\begin{abstract}
% The abstract goes here. DO NOT USE SPECIAL CHARACTERS, SYMBOLS, OR MATH IN YOUR TITLE OR ABSTRACT.
Properly citing academic publications that describe software libraries and algorithms is
the way that open source scientific library users ``pay'' to use the
free software. With large multifaceted libraries and applications that
use several such libraries, even the conscientious user ends up citing
publications in error or missing relevant publications. Some open
source developers list appropriate citations on their website or in
their documentation. Based on a recent addition to the PETSc numerical
software libraries, we suggest an alternative model where the library
itself generates the bibtex items based on {\bf exactly} what
algorithms and portions of the code are used in the application.
\end{abstract}


The PETSc numerical libraries \cite{petsc-web-page,efficient,s2011}
implement hundreds of published algorithms and can use over 50
optional external software packages.
When users publish results based on a simulation
involving PETSc, how do they know what papers they should cite
as relevant and essential to their simulation? We have recently added a
new feature to PETSc to make this determination
straightforward. PETSc contains the function 
\begin{verbatim}
   PetscCitationsRegister(const char bibtexentry[],bool *set)
\end{verbatim}
 which
may be called anywhere in the source code to indicate that a fragment
of code or algorithm is being run that is worthy of a specific
citation. If the application is run with the {\tt -citations
  [filename]} option, then at the conclusion of the simulation all
indicated bibtex entries are printed and available for the user. The
{\tt set} flag is used so that each bibtex item gets recorded
only once. Software packages and algorithms that are compiled into the
library, but are not used in the simulation do not get
listed. For optional external packages to which PETSc interfaces, 
such as hypre~\cite{hypre:homepage}, we call {\tt
  PetscCitationsRegister()} in the ``wrapper'' function that calls the
underlying hypre library, and we cite the publication requested by the
hypre developers. {\tt PetscCitationsRegister()} calls can also be
incorporated directly into appropriate places in the application
code. In addition, if parts of the libraries or application codes are
generated by external source code generators, such as Orio \cite{hartono2009annotation},
these generators can also insert appropriate citations.

We believe approaches such as this should be adopted by the entire open
source scientific software community to ensure that full and accurate
citations are made for libraries used in scientific applications. The
current hit-or-miss approach generally results in many fewer
citations than are appropriate for many scientific software libraries.
This situation can have detrimental effects on funding, future library
development, and even scientific careers.

We have chosen a simple initial implementation of this idea in
PETSc. We store the entire bibtex item, as a C character string, for
each citation in a linked list that is then traversed and printed at
the conclusion of the run. A disadvantage of this strategy is that the
publication information is hardwired into the source code and may not
get updated upon publication change. An alternative would be to store
a ``universal identifier'' for each citation and either translate that
identifier at the conclusion of the run or require the user to
translate the identifiers themselves. Because requiring the application
simulation code to have access to a database of universal
identifiers (in order to do the translation at runtime) poses
difficulties, for example, on large batch systems with no network
access, we did not adopt that approach. Requiring the user
to translate from universal identifiers to the actual bibtex entries
presents one more hurdle that needs to be passed for correct citations,
and we believe that the fewer hurdles the better.

In addition to registering the bibtex data, it may make sense to
register other information---for example, the portion of the source code
related to the citation, the change-set of the source code that
introduced the cited functionality, or notes on the
implementation. We have not explored such possibilities.

We hope this paper opens a discussion into automated efficient ways of
ensuring the proper citation of software libraries and algorithms in
scientific publications. We conclude with a simple demonstration of this 
capability in PETSc using two alternative external packages for solving linear systems,
hypre~\cite{hypre:homepage} and SuperLU~\cite{superlu99}.

{\tiny
\begin{verbatim}
~/Src/petsc/src/snes/examples/tutorials  master $ ./ex19 -ksp_monitor -citations -pc_type hypre -pc_hypre_type boomeramg
lid velocity = 0.0625, prandtl # = 1, grashof # = 1
    0 KSP Residual norm 2.465542831974e-01 
    1 KSP Residual norm 1.263805353152e-02 
    2 KSP Residual norm 1.438657128267e-03 
    3 KSP Residual norm 4.175579235181e-05 
    4 KSP Residual norm 1.967044070973e-06 
    0 KSP Residual norm 2.365268970528e-05 
    1 KSP Residual norm 1.296077770265e-06 
    2 KSP Residual norm 1.105601409840e-07 
    3 KSP Residual norm 1.152875256249e-09 
    4 KSP Residual norm 6.113550034643e-11 
Number of SNES iterations = 2
If you publish results based on this computation please cite the following:
===========================================================================
@TechReport{petsc-user-ref,
  Author = {Satish Balay and Jed Brown and Kris Buschelman and Victor Eijkhout and William D.  Gropp and 
            Dinesh Kaushik and Matthew G. Knepley and Lois Curfman McInnes and Barry F. Smith and Hong Zhang},
  Title = {{PETS}c Users Manual},
  Number = {ANL-95/11 - Revision 3.4},
  Institution = {Argonne National Laboratory},
  Year = {2013}
}
@InProceedings{petsc-efficient,
  Author = {Satish Balay and William D. Gropp and Lois Curfman McInnes and Barry F. Smith},
  Title = {Efficient Management of Parallelism in Object Oriented Numerical Software Libraries},
  Booktitle = {Modern Software Tools in Scientific Computing},
  Editor = {E. Arge and A. M. Bruaset and H. P. Langtangen},
  Pages = {163--202},
  Publisher = {Birkh{\"{a}}user Press},
  Year = {1997}
}
@manual{hypre-web-page,
  title  = {{\sl hypre}: High Performance Preconditioners},
  organization = {Lawrence Livermore National Laboratory},
  note  = {\url{http://www.llnl.gov/CASC/hypre/}}
}
===========================================================================
~/Src/petsc/src/snes/examples/tutorials  master $ ./ex19 -ksp_monitor -citations -pc_type lu -pc_factor_mat_solver_package superlu
lid velocity = 0.0625, prandtl # = 1, grashof # = 1
    0 KSP Residual norm 2.358581702743e-01 
    1 KSP Residual norm 7.147839725241e-17 
    0 KSP Residual norm 2.309061316849e-05 
    1 KSP Residual norm 2.989519344266e-21 
Number of SNES iterations = 2
If you publish results based on this computation please cite the following:
===========================================================================
...
@article{superlu99,
  author  = {James W. Demmel and Stanley C. Eisenstat and John R. Gilbert and Xiaoye S. Li and Joseph W. H. Liu},
  title = {A supernodal approach to sparse partial pivoting},
  journal = {SIAM J. Matrix Analysis and Applications},
  year = {1999},
  volume  = {20},
  number = {3},
  pages = {720-755}
}
===========================================================================
\end{verbatim}
}

\section*{Acknowledgments}
The authors were supported by the
 U.S. Department of Energy, Office of Science, Advanced Scientific Computing Research under Contract DE-AC02-06CH11357.

\bibliographystyle{siam}
\bibliography{petsc-fix,petscapp-fix}

\newpage
{\bf Government License.} The submitted manuscript has been created by UChicago Argonne, LLC,
Operator of Argonne National Laboratory (``Argonne'').
Argonne, a U.S. Department of Energy Office of Science laboratory, is
operated under Contract No. DE-AC02-06CH11357. The U.S. Government
retains for itself, and others acting on its behalf, a paid-up
nonexclusive, irrevocable worldwide license in said article to reproduce,
prepare derivative works, distribute copies to the public, and perform
publicly and display publicly, by or on behalf of the Government.


\end{document}

