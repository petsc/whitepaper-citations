

paper.pdf: bibfiles paper.tex 
	grep -v ' *url *=' petscapp.bib > petscapp-fix.bib
	grep -v ' *url *=' petsc.bib > petsc-fix.bib
	pdflatex paper
	BSTINPUTS=${BSTINPUTS}:./IEEEtranBST/ BIBINPUTS=${BIBINPUTS}:.:./IEEEtranBST/ bibtex paper
	pdflatex paper
	pdflatex paper

bibfiles:
	@if [ ! -e petscapp.bib ] ; then \
          if [ "${PETSC_DIR}" != "" ] ; then \
            ln -s ${PETSC_DIR}/src/docs/tex/petscapp.bib petscapp.bib ; \
            ln -s ${PETSC_DIR}/src/docs/tex/petsc.bib petsc.bib; \
          else  \
            echo "Set the environmental variable PETSC_DIR and run make again"; \
            false ; \
          fi;\
        fi

clean:
	rm -f paper.pdf *.aux *.bbl *.blg *.log *.out *.toc *.tmp

